import requests
import hashlib
import pandas as pd 
import pdb

class Marvel():

    def __init__(self):
        # Columns to extract from dataframe 
        self.columns = ['id', 'name', 'description', 'comics', 'series', 'stories', 'events']
        # base url to get characters 
        self.url = 'http://gateway.marvel.com/v1/public/characters?'
        # Information required for authentication (public_key, private_key and ts)
        self.public_key = '653fddc4ea9e3c7b9d1477f12bad58ce'
        self.private_key = '568093440aa6c703ae373cb71f364ee3dd406d52'
        self.ts = '1'
        # Hash used for authentication, uses public key, private key and timestamp values to create hash using MD5 
        self.hash_md5 = hashlib.md5((self.ts + self.private_key + self.public_key).encode()).hexdigest()
        # Columns we need to get the view totals 
        self.columns_get_number = ['comics', 'series', 'stories', 'events'] 

    # Request function for Marvel API 
    def __get_response_json(self, url):
        link = url + 'ts=' + self.ts + '&apikey=' + self.public_key + '&hash=' + self.hash_md5
        response = requests.get(link)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            response.raise_for_status()

    # Function that changes the indexing, because when you concatenate the dataframes, 
    # the indexing is repeated 
    def __change_indexing(self, df):
        df.reset_index(drop=True, inplace=True)
        return df

    # Function that separates the number of times each hero appeared in comics, series, stories and events
    def __get_number_of_views(self, df, column):
        if column in df:
            df_aux = df[column].apply(lambda s: pd.Series(s))
            df['number'] = df_aux['available']
        df = df.drop(columns=[column])
        df.rename(columns={'number': column}, inplace=True)
        del df_aux
        return df

    # Function that returns a dataframe with data extracted from the API, cleaned and 
    # transformed into the requested format 
    def get_df(self):
        # Make the request to the Marvel website
        response_json = self.__get_response_json(self.url)
        # Get in json the total of super heroes to be extracted
        total_super_heroes  = int(response_json['data']['total'])
        # Get the information regarding the super heroes from json
        super_heroes  = response_json['data']['results']
        # Turns the json extracted from the API into a dataframe with only the columns 
        # defined in the class's init 
        df = pd.DataFrame(super_heroes, columns=self.columns)
        # Code snippet responsible for paging
        while len(df) < total_super_heroes:
            print("Extracting data, currently extracted ",len(df)," out of ",total_super_heroes," super heroes")
            # url with offset to paginate
            url_pagination = self.url +'offset=' + str(len(df)) + '&'
            # Make the request to the Marvel website
            response_json = self.__get_response_json(url_pagination)
            super_heroes = response_json['data']['results']
            # Turns the json extracted from the API into a dataframe with only the columns 
            # defined in the class's init 
            df2 = pd.DataFrame(super_heroes, columns=self.columns)
            # Concatenates the original dataframe with the pagination dataframe
            df = pd.concat([df,df2])

        # Get the number of times each hero appeared in comics, series, stories and events
        for column in self.columns_get_number:
            df = self.__get_number_of_views(df, column)

        # Changes the indexing
        df = self.__change_indexing(df)
        
        return df

# Just init the class and call the get_df function to receive the dataframe containing all characters
# with id, name, description and number of times it appeared in comics, series, stories and events
df = Marvel().get_df()
print(df)