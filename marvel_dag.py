from airflow.models.dag import DAG
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import PythonOperator
from datetime import datetime,timedelta

import os
import sys

module_root = os.path.join(os.path.dirname(__file__),'..')
sys.path.insert(0, os.path.abspath(module_root))

from marvel import Marvel

dag_name = 'z-tech_dag'

default_args = {
    'owner':'airflow',
    'depends_on_past':False,
    'start_date':datetime(2021,7,9),
    'on_failure_callback': task_fail_alert,
    'retries':1,
    'retry_delay':timedelta(minutes=1)
}


dag = DAG(dag_id=dag_name,default_args=default_args,schedule_interval= '0 9 * * *', catchup=False,max_active_runs=1,tags=['atestado'])


start_task = DummyOperator(
    task_id='start_task',
    dag=dag
)

get_dataframe_from_marvel_API = PythonOperator(
    task_id='get_dataframe_from_marvel_API',
    python_callable = Marvel().get_df(),
    dag=dag
)

start_task >> get_dataframe_from_marvel_API
